import { TodoStatusEnum } from "../enum/todo-status.enum"
import { CreateTodoDto } from "./create-todo.dto"

export class UpdateTodoDto extends CreateTodoDto{
    statut : TodoStatusEnum;
}