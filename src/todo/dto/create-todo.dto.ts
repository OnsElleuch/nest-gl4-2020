import { IsNotEmpty } from "class-validator";

export class CreateTodoDto {
    @IsNotEmpty({message: '$property should not be empty '})
    name: string;
    @IsNotEmpty()
    description: string;
}